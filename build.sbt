name := "london-traffic"

version := "1.0"

scalaVersion := "2.11.7"


libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-contrib" % "2.3.13",
  "com.typesafe.akka" %% "akka-testkit" % "2.3.13" % "test",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  "joda-time" % "joda-time" % "2.8.2",
  "org.specs2" %% "specs2-core" % "3.6.5" % "test",
  "org.specs2" % "specs2-mock_2.11" % "3.6.5" % "test",
  "org.mockito" % "mockito-core" % "1.9.5" % "test"
)