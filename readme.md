# London traffic solution overview
Decide to go for an akka implemenation where there is one Orchestrator and two robot actors, akka is a good choice 
for this situation. It gives the flxibility to distribute the work accross remote node later if needed be.

On start-up the robot file position are loaded to the Orchetrator actor and send two positions first positions to go to.
I choose to load the position in a Queue, so that the orchestrator can dequeue and send messages when robot is Ready to receive new ones.

Given that the robot has a limited capacity of number of messages that will receive in any time, I made the architecture 
such that the Robot pull for new destination to go to when:

1. It receive a new destination and it's queue is less than 10
2. When it's queue it is empty, it lookup via the ActorPath the Orchestrator and ask for a new position

Traffic information sent to a Topic DistributedPubSubMediator so that any component in the system can listen to the traffic information

> In my current design, due to time limitation, as improvement is to make the robot faster at moving from one position
> to the next.

Also, I would like to add later, Orchestrator Ack, if message delivery guarantee is important for the system, 
and change the communication between robot and orchestrator to push, instead of pull.  

To run the simulation, current default robot speed is 1000m/seconds
```
 activator compile run
```

