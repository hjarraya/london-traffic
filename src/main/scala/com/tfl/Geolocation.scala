package com.tfl

/**
 * http://www.movable-type.co.uk/scripts/latlong.html
 * Created by hjarraya on 27/10/2015.
 */
object Geolocation {
  val R: Int = 6371

  def distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double = {
    val latDistance: Double = deg2rad(lat2 - lat1)
    val lonDistance: Double = deg2rad(lon2 - lon1)
    val a: Double = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2)
    val c: Double = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    var distance: Double = R * c * 1000
    distance = Math.pow(distance, 2)
    Math.sqrt(distance)
  }

  private def deg2rad(deg: Double): Double = {
    (deg * Math.PI / 180.0)
  }
}
