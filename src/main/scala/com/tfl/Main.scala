package com.tfl

import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.collection.mutable

/**
 * Created by hjarraya on 28/10/2015.
 */
object Main {
  val pattern = "yyyy-MM-dd hh:mm:ss";

  def main(args: Array[String]): Unit = {

    val stations = loadStations("src/main/resources/tube/tube.csv")
    val robot1Position = loadPositions("src/main/resources/robot/5937.csv")
    val robot2Position = loadPositions("src/main/resources/robot/6043.csv")
    val system = ActorSystem("london-traffic", ConfigFactory.load())
    val bankPosition = Position(51.513347, -0.089, DateTime.now())
    //we assume that both robots start at the same bank-station position, also increase speed to iterate quicker over the positions
    val robot1 = system.actorOf(Props(classOf[Robot], "5937", bankPosition, 1000.0d, stations),name = "5937")
    val robot2 = system.actorOf(Props(classOf[Robot], "6043", bankPosition, 1000.0d, stations), name = "6043")

    val robotPaths = Map(("5937",(robot1, robot1Position)), ("6043",(robot2, robot2Position)))

    val orchestrator = system.actorOf(Props(classOf[Orchestrator], robotPaths), name = "orchestrator")
  }

  def loadPositions(filename: String): mutable.Queue[Position] = {
    val l = scala.io.Source.fromFile(filename).getLines()
      .map(ll => { val elements = ll.split(",")
        Position(elements(1).replaceAll("\"", "").toDouble,
          elements(2).replaceAll("\"", "").toDouble,
          DateTime.parse(elements(3).replaceAll("\"", ""), DateTimeFormat.forPattern(pattern)))
      }).toList.reverse
    scala.collection.mutable.Queue(l: _*)
  }

  def loadStations(filename: String): List[Station] = {
    scala.io.Source.fromFile(filename).getLines()
      .map(ll => { val elements = ll.split(",")
        Station(elements(0).replaceAll("\"", ""), elements(1).replaceAll("\"", "").toDouble, elements(2).replaceAll("\"", "").toDouble)
      }).toList
  }
}
