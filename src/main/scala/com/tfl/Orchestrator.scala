package com.tfl

import akka.actor.{ActorRef, ActorLogging, Actor}
import com.tfl.Orchestrator.{StopSimulation, Start}
import com.tfl.Robot.RequestDestination
import org.joda.time.DateTime

import scala.collection.mutable
import scala.concurrent.duration._

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by hjarraya on 27/10/2015.
 */
object Orchestrator {

  case object StopSimulation

  case object Start

}

class Orchestrator(robotsPath: Map[String, (ActorRef, mutable.Queue[Position])]) extends Actor with ActorLogging {

  override def preStart(): Unit = {
    context.system.scheduler.scheduleOnce(5 seconds, self, Start)
    //schedule simulation stopping
    val currentTime = DateTime.now()
    val stopTime = currentTime.withHourOfDay(8).withMinuteOfHour(10).withSecondOfMinute(0).withMillisOfSecond(0)
    if (currentTime.isBefore(stopTime)) {
      log.info("[Orchestrator] current time < 8:10 am ")
      val duration = new org.joda.time.Duration(currentTime, stopTime).getStandardSeconds
      context.system.scheduler.scheduleOnce(duration seconds, self, StopSimulation)
    } else {
      log.info("[Orchestrator] current time > 8:10 am ")
      val duration = new org.joda.time.Duration(currentTime, stopTime.plusDays(1)).getStandardSeconds
      context.system.scheduler.scheduleOnce(duration seconds, self, StopSimulation)
    }
  }

  override def receive: Receive = {
    case Start => {
      robotsPath.keySet.foreach( robotId => {
        for {
          (robotRef, queue) <- robotsPath.get(robotId)
        } yield sendPosition(robotId, robotRef, queue.dequeue())
      })
    }
    case RequestDestination(robotId) => {
      log.info(s"[Orchestrator] robot $robotId requesting a new destination")
      for {
        (robotRef, queue) <- robotsPath.get(robotId)
        if !queue.isEmpty
      } yield {
        log.info(s"[Orchestrator] sending to $robotId queueSize "+queue.size)
        sendPosition(robotId, robotRef, queue.dequeue())
      }
    }
    case StopSimulation => {
      log.info(s"[Orchestrator] stopping simulation ")
      robotsPath.keySet.foreach( robotId => {
        for {
          (robotRef, queue) <- robotsPath.get(robotId)
        } yield robotRef ! StopSimulation
      })
      context.stop(self)
    }
  }

  def sendPosition(id:String, robot:ActorRef, position: Position) = {
    log.info(s"[Orchestrator] sending $position to robot $id")
    robot ! position
  }
}
