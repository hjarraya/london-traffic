package com.tfl

import org.joda.time.DateTime

/**
 * Created by hjarraya on 27/10/2015.
 */
case class Position(latitude:Double,longitude:Double,time:DateTime) {
  def distance(that: Position): Double = Geolocation.distance(this.latitude, this.longitude, that.latitude, that.longitude)
}