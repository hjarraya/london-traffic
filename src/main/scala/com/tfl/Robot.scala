package com.tfl

import akka.actor.{ActorRef, Actor, ActorLogging}
import akka.contrib.pattern.{DistributedPubSubMediator, DistributedPubSubExtension}
import com.tfl.Orchestrator.StopSimulation
import com.tfl.Robot.{RequestDestination, Tick}
import org.joda.time.DateTime
import scala.collection.mutable
import scala.concurrent.duration._

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by hjarraya on 27/10/2015.
 */
object Robot {

  case object Tick

  case object Busy

  case class RequestDestination(id: String)

}

class Robot(id: String, startPosition: Position, speed: Double, stations: List[Station]) extends Actor with ActorLogging {

  val mediator = DistributedPubSubExtension(context.system).mediator

  val destinations = mutable.Queue[Position]()
  var currentPosition = startPosition
  var movingTo = startPosition
  var lastTimeUpdate = DateTime.now()

  override def preStart(): Unit = {
    context.system.scheduler.schedule(0 seconds,15 seconds, self, Tick)
  }

  override def receive: Receive = {
    case destination: Position => {
      log.info(s"[Robot $id] received new destination $destination")
      destinations.enqueue(destination)
      askMoreDestination(sender())
      lastTimeUpdate = DateTime.now()
      val candidate = destinations.front
      log.info(s"[Robot $id] $currentPosition and candidate $candidate "+ candidate.distance(currentPosition) + "meters")
      movingTo = candidate
      context.become(moving)
    }
    case Tick =>
      log.info(s"[Robot $id] received Tick message while stopped in $currentPosition")
      if (!destinations.isEmpty) {
        lastTimeUpdate = DateTime.now()
        val destination = destinations.front
        movingTo = destination
        context.become(moving)
      } else {
        log.info(s"[Robot $id] empty queue asking for more destinations")
        context.actorSelection("/user/orchestrator") ! RequestDestination(id)
      }

    case StopSimulation =>
      log.info(s"[Robot $id] received Stop simulation message $currentPosition")
      context.stop(self)
  }

  import scala.util.Random

  val trafficStatus = List("HEAVY", "LIGHT", "MODERATE")
  val delta = 0.001

  def moving: Receive = {
    case Tick =>
      val eta: Double = currentPosition.distance(movingTo) / speed
      log.info(s"[Robot $id] received Tick message while moving to $movingTo eta "+Math.max(eta, 5)+" seconds Queue size "+destinations.size)
      if (eta < delta || (DateTime.now().getMillis - (lastTimeUpdate.getMillis + eta)) > 0) {
        log.info(s"[Robot $id] arrived at destination $movingTo")
        stations.filter(s => (s.distance(currentPosition) - 350) <= 0)
          .foreach(s => {
            val trafficInfo = TrafficInfo(id, DateTime.now(), speed, Random.shuffle(trafficStatus).head)
            log.info(s"[Robot $id] publishing traffic Info $trafficInfo")
            mediator ! DistributedPubSubMediator.Publish("TrafficInfo", trafficInfo)
          })
        destinations.dequeue
        currentPosition = movingTo
        context.unbecome()
      } else {
        // robot did not arrive to location yet reschedule tick
      }
      lastTimeUpdate = DateTime.now()

    case destination: Position =>
      log.info(s"[Robot $id] received new destination $destination while moving")
      destinations.enqueue(destination)
      askMoreDestination(sender())
  }

  def askMoreDestination(sender: ActorRef) = {
    if (destinations.size < 9) {
      log.info(s"[Robot $id] requesting more destinations")
      sender ! RequestDestination(id)
    }
  }

}
