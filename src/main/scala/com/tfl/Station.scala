package com.tfl

/**
 * Created by hjarraya on 27/10/2015.
 */
case class Station(name: String, latitude: Double, longitude: Double) {

  def distance(pos: Position): Double = Geolocation.distance(this.latitude, this.longitude, pos.latitude, pos.longitude)

  def distance(that: Station): Double = Geolocation.distance(this.latitude, this.longitude, that.latitude, that.longitude)

}
