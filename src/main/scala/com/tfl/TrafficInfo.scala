package com.tfl

import org.joda.time.DateTime

/**
 * Created by hjarraya on 27/10/2015.
 */
case class TrafficInfo(id:String,time:DateTime,speed:Double, condition:String)
