package com.tfl

import akka.actor.{Props, ActorSystem}
import akka.testkit.{TestProbe, ImplicitSender, TestKit}
import com.tfl.Orchestrator.Start
import com.tfl.Robot.RequestDestination
import com.typesafe.config.ConfigFactory
import org.joda.time.DateTime
import org.scalatest.BeforeAndAfterAll
import org.specs2.mutable.SpecificationLike
import scala.concurrent.duration._

import scala.collection.mutable

/**
 * Created by hjarraya on 28/10/2015.
 */
class OrchestratorTest extends TestKit(ActorSystem("test-system",ConfigFactory
  .parseString("akka.remote.netty.tcp.hostname=127.0.0.1")
  .withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=0"))
  .withFallback(ConfigFactory.load()))) with ImplicitSender with SpecificationLike   {
  sequential

  val bankPosition = Position(51.513347, -0.089, DateTime.now())
  val barbicanPosition = Position(51.520215,-0.097722, DateTime.now())

  "An orchestrator " should {
    "Dequeue and send new position on start up" in {
      val queue = mutable.Queue(bankPosition, barbicanPosition)
      val robotActor = TestProbe()
      val rebotPath = Map(("robotId",(robotActor.ref, queue)))
      val orchestrator = system.actorOf(Props(new Orchestrator(rebotPath)))
      orchestrator.tell(Start, testActor)
      robotActor.expectMsg(10 seconds, bankPosition)
      success
    }

    "Dequeue and send new position when request message received" in {
      val queue = mutable.Queue(bankPosition, barbicanPosition)
      val robotActor = TestProbe()
      val rebotPath = Map(("robotId",(robotActor.ref, queue)))
      val orchestrator = system.actorOf(Props(new Orchestrator(rebotPath)))
      orchestrator.tell(RequestDestination("robotId"), testActor)
      robotActor.expectMsg(10 seconds, bankPosition)
      shutdown()
      success
    }
  }


}
