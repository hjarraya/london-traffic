package com.tfl

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.contrib.pattern.DistributedPubSubExtension
import akka.contrib.pattern.DistributedPubSubMediator.Subscribe
import akka.testkit.{TestActors, TestProbe, ImplicitSender, TestKit}
import com.tfl.Robot.{Tick, RequestDestination}
import com.typesafe.config.ConfigFactory
import org.joda.time.DateTime
import org.scalatest.BeforeAndAfterAll
import org.specs2.mutable.SpecificationLike
import scala.concurrent.duration._

/**
 * Created by hjarraya on 27/10/2015.
 */
class ForwardActor(ref: ActorRef) extends Actor {
  override def receive = {
    case message ⇒ ref forward message
  }
}

class RobotTest extends
TestKit(ActorSystem("test-system", ConfigFactory.parseString("akka.remote.netty.tcp.hostname=127.0.0.1")
  withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=0"))
  .withFallback(ConfigFactory.load()))) with ImplicitSender with SpecificationLike {
  sequential

  val bankStation = Station("Bank", 51.513347, -0.089)
  val bankPosition = Position(51.513347, -0.089, DateTime.now())
  val barbicanStation = Station("Barbican",51.520215,-0.097722)

  val nortPoleStation = Position(90, 0, DateTime.now())

  "A Robot actor " should {
    "Request for destinations from sender when it receive a destination and queue is not busy" in {
      val robot = system.actorOf(Props(new Robot(id = "testRobot", startPosition = bankPosition, speed = 10, stations = List(bankStation, barbicanStation))))
      robot.tell(bankPosition, testActor)
      expectMsg(30 seconds, RequestDestination("testRobot"))
      success
    }

    "public traffic info when it arrives near a station" in {
      val robot = system.actorOf(Props(new Robot(id = "testRobot", startPosition = bankPosition, speed = 10, stations = List(bankStation, barbicanStation))))
      val mediator = DistributedPubSubExtension(system).mediator
      val testSubscriber = TestProbe()
      mediator ! Subscribe("TrafficInfo", testSubscriber.ref)
      robot.tell(bankPosition, testActor)
      testSubscriber.expectMsgPF(30 seconds) {
        case TrafficInfo("testRobot",_,_,_) => success
        case _ => failure
      }
    }

    "when queue is empty and receive Tick message, ask orchestrator for a new destination" in {
      val robot = system.actorOf(Props(new Robot(id = "testRobot", startPosition = bankPosition, speed = 10, stations = List(bankStation, barbicanStation))))
      val probe = TestProbe()
      val fwd = system.actorOf(Props(new ForwardActor(probe.ref)), "orchestrator")
      robot.tell(Tick, testActor)
      probe.expectMsg(30 seconds, RequestDestination("testRobot"))
      shutdown()
      success
    }
  }
}
