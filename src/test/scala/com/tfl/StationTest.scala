package com.tfl

import org.specs2.mutable.Specification

/**
 * Created by hjarraya on 27/10/2015.
 */
class StationTest extends Specification {

  val delta = 0.001

  "Geolocation between two stations" should {
    "be calculated returned meters" in {
      val p1 = Station("Aldgate",51.514342,-0.075627)
      val p2 = Station("Aldgate East",51.51503,-0.073162)

      Math.abs(p1.distance(p2) - 186.943)  must lessThan(delta)
    }

    "close coordinates " in {
      val p1 = Station("x1",51.510983,-0.108073)
      val p2 = Station("x2",51.510983,-0.108073)
      println(p1.distance(p2))
      Math.abs(p1.distance(p2))  must lessThan(delta)
    }

  }
}
